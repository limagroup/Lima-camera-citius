//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "processlib/LinkTask.h"

#include "CitiusInterface.h"
#include "CitiusCamera.h"
#include "CitiusFileReader.h"

using namespace lima;
using namespace lima::Citius;
/*****************************************************************************
			    Detector Info
****************************************************************************/
static const double DETETCTOR_FREQUENCY = 17400.;

DetInfoCtrlObj::DetInfoCtrlObj(Camera& camera) : m_cam(camera)
{
}

DetInfoCtrlObj::~DetInfoCtrlObj()
{
}

void DetInfoCtrlObj::getMaxImageSize(Size& max_image_size)
{
  max_image_size = Size(384,728);
}

void DetInfoCtrlObj::getDetectorImageSize(Size& det_image_size)
{
  det_image_size = Size(384,728);
}

void DetInfoCtrlObj::getDefImageType(ImageType& def_image_type)
{
  def_image_type = Bpp32F;
}

void DetInfoCtrlObj::getCurrImageType(ImageType& curr_image_type)
{
  curr_image_type = Bpp32F;
}

void DetInfoCtrlObj::setCurrImageType(ImageType curr_image_type)
{
  DEB_MEMBER_FUNCT();
  THROW_HW_ERROR(Error) << "Not managed!!";
}

void DetInfoCtrlObj::getPixelSize(double& x_size,double& y_size)
{
  //@todo check pixel size
  x_size = y_size = -1.;
}

void DetInfoCtrlObj::getDetectorType(std::string& det_type)
{
  det_type = "Citius";
}

void DetInfoCtrlObj::getDetectorModel(std::string& det_model)
{
  det_model = "384*728";
}

/******************************************************************************
			    Sync inteface
****************************************************************************/

SyncCtrlObj::SyncCtrlObj(Camera& cam) : m_cam(cam)
{
}

SyncCtrlObj::~SyncCtrlObj()
{
}

void SyncCtrlObj::prepareAcq()
{
  int total_frames = m_nb_acc_frames * m_nb_frames;
  m_cam.set_nb_acq_frames(total_frames,m_nb_frames);
  m_cam.set_nb_filewriter_train(1);
}

bool SyncCtrlObj::checkTrigMode(TrigMode trig_mode)
{
    bool valid_mode = false;
    switch (trig_mode)
    {
    case IntTrig:
      //case ExtGate:
        valid_mode = true;
        break;

    default:
        valid_mode = false;
        break;
    }
    return valid_mode;
}

void SyncCtrlObj::setTrigMode(TrigMode trig_mode)
{
  DEB_MEMBER_FUNCT();
  if (!checkTrigMode(trig_mode))
    throw LIMA_HW_EXC(InvalidValue, "Invalid trigger mode");
  //Don't know yet how to manage triggers.
}

void SyncCtrlObj::getTrigMode(TrigMode& trig_mode)
{
  //For now only IntTrig
  trig_mode = IntTrig;
}

void SyncCtrlObj::setExpTime(double exp_time)
{
  /** Exposure time with this detetcor is an 
   *  Accumulation of frames taken at 17.4kHz.
   *  
   */
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(exp_time);
  m_nb_acc_frames = exp_time * DETETCTOR_FREQUENCY;
  DEB_RETURN() << DEB_VAR1(m_nb_acc_frames);
  
  if(m_nb_acc_frames <= 0)
    {
      m_nb_acc_frames = 1;
      THROW_HW_ERROR(Error) << "Can't go that fast";
    }
  m_cam.set_nb_sum_frames(m_nb_acc_frames);
}

void SyncCtrlObj::getExpTime(double& exp_time)
{
  exp_time = m_nb_acc_frames / DETETCTOR_FREQUENCY;
}

void SyncCtrlObj::setLatTime(double lat_time)
{
  DEB_MEMBER_FUNCT();
  if(lat_time != 0.)
    throw LIMA_HW_EXC(InvalidValue, "Latency not managed");
}

void SyncCtrlObj::getLatTime(double& lat_time)
{
  lat_time=0.;		// @todo to be checked
}

void SyncCtrlObj::setNbHwFrames(int nb_frames)
{
  m_nb_frames = nb_frames;
}

void SyncCtrlObj::getNbHwFrames(int& nb_frames)
{
  nb_frames = m_nb_frames;
}

void SyncCtrlObj::getValidRanges(ValidRangesType& valid_ranges)
{
  double min_time = 1./DETETCTOR_FREQUENCY;
  double max_time = 1e6;
  valid_ranges.min_exp_time = min_time;
  valid_ranges.max_exp_time = max_time;
  valid_ranges.min_lat_time = 0;
  valid_ranges.max_lat_time = 0;
}
/******************************************************************************
			 Reconstruction Task
****************************************************************************/

class lima::Citius::_ReconstructionTask: public LinkTask
{
  DEB_CLASS_NAMESPC(DebModCamera, "Reconstruction", "Citius");
public:
  class CBK : public Buffer::Callback
  {
  public:
    CBK(_ReconstructionTask& task) : m_task(task) {}
    virtual void destroy(void *dataPt)
    {
      m_task.release(dataPt);
    }
  private:
    _ReconstructionTask& m_task;
  };
  
    _ReconstructionTask(BufferMgr& mgr) :
      m_buffer(mgr),m_cbk(*this) {}
  
  virtual Data process(Data &aData);
  void release(void *dataPt);
private:
  BufferMgr& m_buffer;
  CBK m_cbk;
};

Data _ReconstructionTask::process(Data &aData)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(aData);
  
  Data aReturnData = aData;
  
  AutoMutex lock(m_buffer.m_cond.mutex());
  auto frame_info_it = m_buffer.m_frames.find(aData.frameNumber);
  if(frame_info_it == m_buffer.m_frames.end())
    throw ProcessException("Citius Reconstruction: can not find the raw frame");
  auto frame_info = frame_info_it->second;
  lock.unlock();
  
  // Allocate a new buffer for final frame
  Buffer *aNewBuffer = new Buffer(aData.size());
  DEB_TRACE() << DEB_VAR2(aNewBuffer,aData.size());
  aReturnData.setBuffer(aNewBuffer);
  aNewBuffer->unref();

  for(auto it = frame_info->m_phy_frames.begin();
      it != frame_info->m_phy_frames.end();++it)
    {
      int phy = it->first / 2 * 2;
      if(phy < 4)		// first block are full
	{
	  int block_size = FrameInfo::PHY_SIZE * 2;
	  char *dest = (char*)(aNewBuffer->data) + (phy / 2) * block_size;
	  char *src = (char*)it->second.frame_ptr;
	  DEB_TRACE() << "memcpy: " << DEB_VAR3((void*)dest,(void*)src,block_size);
	  memcpy(dest,src,block_size);
	}
      else			// block == 4 + 5
	{
	  /** phy block 4 is == to all other 
	      so just do a memcpy
	  */
	  int block_size = FrameInfo::PHY_SIZE * 2;
	  char *dest = (char*)(aNewBuffer->data) + (phy / 2) * block_size;
	  char *src = (char*)it->second.frame_ptr;
	  DEB_TRACE() << "memcpy: " << DEB_VAR3((void*)dest,(void*)src,block_size/2);
	  memcpy(dest,src,FrameInfo::PHY_SIZE);
	  dest += FrameInfo::PHY_SIZE;
	  
	  /** for phy 5 the block structure is 8*(11 lines + 5 blank lines)
	   */
	  FrameDim &dim = it->second.frame_dim;
	  const Size& size = dim.getSize();

	  int block_step = (11 + 5) * dim.getDepth() * size.getWidth();
	  block_size = 11 * dim.getDepth() * size.getWidth();

	  for(int i = 0;i < 8;++i)
	    {
	      char* src = (((char*)it->second.frame_ptr) + FrameInfo::PHY_SIZE +
			   i * block_step);
	      DEB_TRACE() << "memcpy: " << DEB_VAR3((void*)dest,(void*)src,block_size);
	      memcpy(dest,src,block_size);
	      dest += block_size;
	    }
	}
    }
  
  lock.lock();
  m_buffer.m_frame_in_use.erase(aData.frameNumber);
  m_buffer.m_frame_in_process.insert(aNewBuffer->data);
  aNewBuffer->callback = &m_cbk;
  m_buffer.m_cond.broadcast();
  return aReturnData;
}

void _ReconstructionTask::release(void* dataPt)
{
  AutoMutex lock(m_buffer.m_cond.mutex());
  m_buffer.m_frame_in_process.erase(dataPt);
  m_buffer.m_cond.broadcast();
}
/******************************************************************************
			    Reconstruction
****************************************************************************/
Reconstruction::Reconstruction(BufferMgr& buffer) :
  m_reconstruction_task(new _ReconstructionTask(buffer))
{
}

Reconstruction::~Reconstruction()
{
  m_reconstruction_task->unref();
}

LinkTask* Reconstruction::getReconstructionTask()
{
  return m_reconstruction_task;
}


/******************************************************************************
			      Interface
****************************************************************************/

Interface::Interface(const std::string& url) :
  m_cam(new Camera(url)),
  m_det_info(*m_cam),
  m_sync(*m_cam),
  m_buffer(new BufferMgr(LIMA_TMP_PATH)),
  m_reconstruction(*m_buffer)
{
  m_cap_list.push_back(HwCap(&m_det_info));
  m_cap_list.push_back(HwCap(&m_sync));
  m_cap_list.push_back(HwCap(m_buffer.get()));
  m_cap_list.push_back(HwCap(&m_reconstruction));
}

Interface::~Interface()
{
}

void Interface::getCapList(CapList& cap_list) const
{
  cap_list = m_cap_list;
}

void Interface::reset(ResetLevel reset_level)
{
  // @todo
}

void Interface::prepareAcq()
{
  m_buffer->prepare();
  m_sync.prepareAcq();
  m_cam->prepareAcq();
}

void Interface::startAcq()
{
  m_buffer->start();
  m_cam->startAcq();
}

void Interface::stopAcq()
{
  m_cam->stopAcq();
  m_buffer->stop();
}

void Interface::getStatus(StatusType& status)
{
  bool buffer_running = m_buffer->is_running();
  Camera::AcquisitionStatus cam_status = m_cam->acq_status();
  status.det = cam_status == Camera::READY ? DetIdle : DetExposure;
  status.acq = buffer_running ? AcqRunning : AcqReady;
}

int Interface::getNbHwAcquiredFrames()
{
  return m_buffer->get_acquired_frames();
}
