//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################
#include "CitiusCamera.h"

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

using namespace lima;
using namespace lima::Citius;
using namespace Pistache;

Camera::Camera(const std::string& url) :
  m_base_url(url)
{ 
  DEB_CONSTRUCTOR();
  
  auto opts = Http::Experimental::Client::options().threads(1).maxConnectionsPerHost(2);
  m_client.init(opts);

  std::vector<AsynResponse> responses;

  std::string acq_string;
  _raw_get_value_string("/detector/status/acquisition/state",responses,acq_string);
  std::string cooling_state;
  _raw_get_value_string("/detector/status/cooling/stat",responses,cooling_state);
  std::string interlock_state;
  _raw_get_value_string("/detector/status/interlock/state",responses,interlock_state);


  //Init filewriter;
  std::string error_compression;
  _raw_set_string_value("/filewriter/config/compression",responses,"\"Uncompressed\"",error_compression);

  std::string path = "\""; path += LIMA_EXTRA_PATH;path += "\"";
  std::string error_path;
  _raw_set_string_value("/filewriter/config/output_dir",responses,path,error_path);
  
  _wait_responses(responses);

  if(!error_compression.empty() || !error_path.empty())
    THROW_HW_ERROR(Error) << error_compression << "," << error_path;
  
  // Dispay the status information
  std::cout << "Citius is connected to: *" << url << "*" << std::endl
	    << std::endl << std::endl
	    << "Acquisition state: " << acq_string << std::endl
	    << "Cooling state: " << cooling_state << std::endl
	    << "Interlock state: " << interlock_state << std::endl;
}

Camera::~Camera()
{
  m_client.shutdown();
}

Camera::AcquisitionStatus Camera::acq_status()
{
  std::vector<AsynResponse> responses;
  std::string acq_string;
  _raw_get_value_string("/detector/status/acquisition/state",responses,acq_string);
  _wait_responses(responses);
  return _string_2_acquisition_status(acq_string);
}

Camera::CoolingStatus Camera::cooling_status()
{
  std::vector<AsynResponse> responses;
  std::string cooling_string;
  _raw_get_value_string("/detector/status/cooling/stat",responses,cooling_string);
  _wait_responses(responses);
  return _string_2_cooling_status(cooling_string);
}

int Camera::get_nb_acq_frames()
{
  std::vector<AsynResponse> responses;
  std::string nb_acq_frames;
  _raw_get_value_string("/detector/config/train_length_dfb",responses,nb_acq_frames);
  _wait_responses(responses);
  return std::stoi(nb_acq_frames);
}

void Camera::set_nb_acq_frames(int sensor_acq_frames,int acq_frames)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR2(sensor_acq_frames,acq_frames);
  
  std::vector<AsynResponse> responses;
  std::string sensor_acq_frames_string = std::to_string(sensor_acq_frames);
  std::string error_sensor_acq_frames_string;
  _raw_set_string_value("/detector/config/train_length_sensor",responses,
			sensor_acq_frames_string,error_sensor_acq_frames_string);
  _wait_responses(responses);
  if(!error_sensor_acq_frames_string.empty())
    THROW_HW_ERROR(Error) << error_sensor_acq_frames_string;

  responses.clear();
  std::string acq_frames_string = std::to_string(acq_frames);
  std::string error_acq_frames_string;
  _raw_set_string_value("/detector/config/train_length_dfb",responses,
			acq_frames_string,error_acq_frames_string);
  _wait_responses(responses);
  if(!error_acq_frames_string.empty())
    THROW_HW_ERROR(Error) << error_acq_frames_string;
}

int Camera::get_nb_sum_frames()
{
  std::vector<AsynResponse> responses;
  std::string sum_frames;
  _raw_get_value_string("/detector/config/sum_nframes",responses,sum_frames);
  _wait_responses(responses);
  return std::stoi(sum_frames);
}

void Camera::set_nb_sum_frames(int sum_frames)
{
  DEB_MEMBER_FUNCT();
  
  std::vector<AsynResponse> responses;
  std::string sum_frames_string = std::to_string(sum_frames);
  std::string error_sum_frames;
  _raw_set_string_value("/detector/config/sum_nframes",responses,
			sum_frames_string,error_sum_frames);
  _wait_responses(responses);
  if(!error_sum_frames.empty())
    THROW_HW_ERROR(Error) << error_sum_frames;
}


int Camera::get_nb_filewriter_train()
{
  std::vector<AsynResponse> responses;
  std::string nb_train;
  _raw_get_value_string("/filewriter/config/ntrains",responses,nb_train);
  _wait_responses(responses);
  return std::stoi(nb_train);
}

void Camera::set_nb_filewriter_train(int nb_train)
{
  DEB_MEMBER_FUNCT();
  
  std::vector<AsynResponse> responses;
  std::string nb_train_string = std::to_string(nb_train);
  std::string error_nb_train;
  _raw_set_string_value("/filewriter/config/ntrains",responses,
			nb_train_string,error_nb_train);
  _wait_responses(responses);
  if(!error_nb_train.empty())
    THROW_HW_ERROR(Error) << error_nb_train;
}


void Camera::prepareAcq()
{
}

void Camera::startAcq()
{
  DEB_MEMBER_FUNCT();
  
  std::vector<AsynResponse> responses;
  std::string error_start;
  _raw_set_string_value("/filewriter/config/mode",responses,"\"start\"",error_start);
  _wait_responses(responses);
  if(!error_start.empty())
    THROW_HW_ERROR(Error) << error_start;
}

void Camera::stopAcq()
{
  DEB_MEMBER_FUNCT();
  
  std::vector<AsynResponse> responses;
  std::string error_stop;
  _raw_set_string_value("/filewriter/config/mode",responses,"\"stop\"",error_stop);
  std::string error_stream_stop;
  _raw_set_string_value("/stream/config/mode",responses,"\"stop\"",error_stream_stop);
  _wait_responses(responses);
  if(!error_stop.empty() || !error_stream_stop.empty())
    THROW_HW_ERROR(Error) << error_stop << " ," << error_stream_stop;
}

Camera::AcquisitionStatus Camera::_string_2_acquisition_status(const std::string& value)
{
  if(value == "ready")
    return Camera::READY;
  else
    return Camera::NOT_READY;
}

Camera::CoolingStatus Camera::_string_2_cooling_status(const std::string& value)
{
  if(value == "not connected")
    return NOT_CONNECTED;
  else if(value == "disabled")
    return DISABLED;
  else if(value == "cooling")
    return COOLING;
  else if(value == "cooled")
    return COOLED;
  else
    return ERROR;
}
void Camera::_raw_get_value_string(const std::string& url_suffix,
				   std::vector<Camera::AsynResponse>& responses,
				   std::string& value_string)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(url_suffix);
  
  std::string page = m_base_url + url_suffix;
  auto resp = m_client.get(page).send();
  resp.then(
	    [&,deb,url_suffix](Http::Response response) mutable
	    {
	      if(response.code() != Http::Code::Ok)
		THROW_HW_ERROR(Error) << "Http Error :( on " << DEB_VAR1(url_suffix);
	      auto body = response.body();
	      if(body.empty())
		THROW_HW_ERROR(Error) << "Response is empty";
	      
	      rapidjson::Document d;
	      d.Parse(body.c_str());
	      if(d.HasParseError())
		THROW_HW_ERROR(Error) << "Response is not Json!!!";
	      
	      if(!d.HasMember("value"))
		THROW_HW_ERROR(Error) << "Expect **value** key";
	      value_string = d["value"].GetString();
	    },
	    [&,deb](std::exception_ptr exc_ptr) mutable
	    {
	      try{std::rethrow_exception(exc_ptr);}
	      catch(const std::exception& exc)
		{
		  THROW_HW_ERROR(Error) << "Get status error: "
					<< exc.what();
		}
	    }
	    );
  responses.push_back(std::move(resp));
}

void Camera::_raw_set_string_value(const std::string& url_suffix,
				   std::vector<Camera::AsynResponse>& responses,
				   const std::string& value_string,
				   std::string& error_msg)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR2(url_suffix,value_string);

  std::string page = m_base_url + url_suffix;
  std::string json_str = "{\"value\": " + value_string + "}";
  auto req_builder = m_client.put(page);
  auto content = Http::Header::ContentType(MIME(Application,Json));
  auto content_shared = std::make_shared<Http::Header::ContentType>(content);
  req_builder.header(content_shared);
  req_builder.body(json_str);
  auto resp = req_builder.send();
  resp.then(
	    [&,url_suffix,value_string](Http::Response response) mutable
	    {
	      if(response.code() != Http::Code::Ok)
		{
		  std::ostringstream output;
		  output << "Http Error :( " << DEB_VAR2(url_suffix,value_string);
		  error_msg = output.str();
		}
	    },
	    [&](std::exception_ptr exc_ptr) mutable
	    {
	      try{std::rethrow_exception(exc_ptr);}
	      catch(const std::exception& exc)
		{
		  std::ostringstream output;
		  output  << "Get status error: " << exc.what();
		  error_msg = output.str();
		}
	    }
	    );
  responses.push_back(std::move(resp));
}

void Camera::_wait_responses(std::vector<AsynResponse>& responses,int timeout)
{
  DEB_MEMBER_FUNCT();

  try
    {
      auto sync = Async::whenAll(responses.begin(), responses.end());
      Async::Barrier<std::vector<Http::Response>> barrier(sync);
      std::cv_status wait_status = barrier.wait_for(std::chrono::seconds(timeout));
      if(wait_status == std::cv_status::timeout)
	THROW_HW_ERROR(Error) << "Timeout to connect to detector";
    }
  catch(...)
    {
      DEB_TRACE() << "Catch an exception";
      responses.clear();
      DEB_TRACE() << "Responses are cleared";
      throw;
    }
}
