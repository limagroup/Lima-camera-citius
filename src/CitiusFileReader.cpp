//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <dirent.h>
#include <poll.h>

#include "CitiusFileReader.h"
using namespace lima;
using namespace lima::Citius;

const static uint32_t WATCH_MASK = IN_CREATE | IN_MODIFY | IN_MOVED_TO | IN_CLOSE_WRITE;
const std::string BufferMgr::FILE_EXTENTION = ".dat";
const int MAX_PROCESSED_FRAMES = 100; // Number of frames queue for process

struct TrainHeadInfo
{
  u_int32_t number;
  u_int16_t version;
  u_int16_t mode;
  u_int64_t size;
  u_int32_t header_size;
  u_int32_t train_id;
  u_int64_t tag_id;
  u_int32_t time_sec;
  u_int32_t time_nanosec;
  u_int32_t num_of_frames;
  u_int16_t tile_info;
  u_int16_t chip_id;
  u_int32_t packet_header_info;
  u_int32_t prb_id;
  u_int64_t tag_offset;
  u_int64_t tag_period;
  u_int32_t num_of_processed_frames;
  u_int32_t num_of_integrations;
  u_int32_t num_of_decimations;
  u_int32_t elecon;
};

struct FrameHeadInfo
{
  u_int32_t magic_number;
  u_int16_t version;
  u_int16_t image_mode;
  u_int64_t data_size;
  u_int32_t header_size;
  u_int32_t frame_id;
  u_int64_t tag_id;
  u_int32_t time_sec;
  u_int32_t time_nanosec;
  u_int64_t tag_offset;
  u_int64_t tag_period;
  u_int32_t add_info;
  u_int32_t prb_id;
  u_int32_t packet_header_info;
  u_int32_t processed_frame_id;
  u_int32_t num_of_integration_frames;
};
  
DataFile::DataFile(const char* filepath, const FrameDim& frame_dim) :
  m_file_path(filepath),
  m_file_fd(-1),
  m_mode(DataFile::UNINITIALIZED),
  m_next_seek(0),
  m_frame_dim(frame_dim),
  m_packet_header_info(-1)
{
}

DataFile::DataFile(DataFile&& other) :
  m_file_path(other.m_file_path),
  m_file_fd(other.m_file_fd),
  m_mode(other.m_mode),
  m_next_seek(other.m_next_seek),
  m_frame_dim(other.m_frame_dim),
  m_packet_header_info(other.m_packet_header_info)
{
  other.m_file_path.clear();
  other.m_file_fd = -1;
}

DataFile::~DataFile()
{
  if(m_file_fd >=0)
    close(m_file_fd);
}

bool DataFile::get_next(std::list<PhyFrameInfo> &frame_list)
{
  DEB_MEMBER_FUNCT();
  switch(m_mode)
    {
    case UNINITIALIZED:
      //try to open the file
      m_file_fd = open(m_file_path.c_str(),O_RDONLY);
      if(m_file_fd < 0)
	THROW_HW_ERROR(Error) << "Can not open file " << m_file_path;
      m_mode = TRAIN;
    case TRAIN:
      {
	TrainHeadInfo train_info;
	ssize_t read_size = read(m_file_fd,&train_info,sizeof(train_info));
	if(read_size != sizeof(train_info))
	  {
	    if(read_size < 0)
	      THROW_HW_ERROR(Error) << "Can not read file " << m_file_path;
	    return false;
	  }
	  	
	m_next_seek = train_info.header_size;
	m_nb_frames_to_read = train_info.num_of_processed_frames;
	m_packet_header_info = train_info.packet_header_info;
	m_mode = FRAME;
      }
    case FRAME:
      {
	long page_size = sysconf(_SC_PAGE_SIZE);
	while(m_nb_frames_to_read)
	  {
	    FrameHeadInfo frame_info;
	    lseek(m_file_fd,m_next_seek,SEEK_SET);
	    ssize_t read_size = read(m_file_fd,&frame_info,sizeof(frame_info));
	    if(read_size != sizeof(frame_info))
	      {
		if(read_size < 0)
		  THROW_HW_ERROR(Error) << "Can not read file " << m_file_path;
		return false; // not ready
	      }

	    //check the size of the file
	    off_t current_file_size = lseek(m_file_fd, 0L, SEEK_END);
	    off_t data_seek = m_next_seek + frame_info.header_size;
	    ssize_t size_to_read = frame_info.data_size - frame_info.header_size;
	    if(current_file_size < data_seek + size_to_read)
	      return false; 	// not ready

	    off_t page_data_seek = (data_seek / page_size) * page_size;
	    off_t frame_data_offset = data_seek - page_data_seek;
	    ssize_t mmap_size = size_to_read + frame_data_offset;
	  
	    void* base_frame_data = mmap(NULL,mmap_size,
					 PROT_READ,MAP_SHARED,m_file_fd,page_data_seek);
	    if(base_frame_data == MAP_FAILED)
	      THROW_HW_ERROR(Error) << "Can not mmap data frame";
	  

	    //create the new frame and add it to the list
	    PhyFrameInfo newFrame(base_frame_data,mmap_size,frame_data_offset,
				  get_phy_id(),m_frame_dim);
	    newFrame.acq_frame_nb = frame_info.processed_frame_id;
	    frame_list.push_back(std::move(newFrame));
	    m_next_seek += frame_info.data_size;
	    --m_nb_frames_to_read;
	  }
      }
      return true;
      break;
    case END:
      return true;
      break;
    default:
      THROW_HW_ERROR(Error) << "Should not be here ;)";
      break;
    }
}

int DataFile::get_phy_id() const
{
  DEB_MEMBER_FUNCT();
  
  switch((m_packet_header_info >> 18) & 0x3f)
    {
    case 3:
      return 0;
    case 48:
      return 4;
    case 12:
      return 2;
    default:
      THROW_HW_ERROR(Error) << "Unknown";
    }
}

std::ostream& lima::Citius::operator<<(std::ostream& os,const DataFile& file)
{
  os << "<"
     << "file_path=" << file.m_file_path << ", "
     << "nb_frames_to_read=" << file.m_nb_frames_to_read << ", "
     << "mode=" << file.m_mode //<< ", "
     << ">";
  return os;
}
/****************************************************************************
			      FrameInfo
****************************************************************************/
FrameInfo::FrameInfo(int acq_frame_number,FrameDim fdim) :
  m_acq_frame_nb(acq_frame_number),
  m_frame_dim(fdim)
{
}

FrameInfo::~FrameInfo()
{
}

bool FrameInfo::is_complete() const
{
  int number_of_phy = ::ceil(m_frame_dim.getMemSize() / PHY_SIZE);
  number_of_phy /= 2;		// one file give 2 PHY
  return m_phy_frames.size() == number_of_phy;
}

void FrameInfo::add(PhyFrameInfo& phy_frame)
{
  DEB_MEMBER_FUNCT();
  
  auto it = m_phy_frames.find(phy_frame.m_phy_id);
  if(it != m_phy_frames.end())
    {
      //THROW_HW_ERROR(Error) @todo not normal :-(
      DEB_WARNING() << "Frame already have this phy frame:" << DEB_VAR1(phy_frame.m_phy_id);
      return;
    }

  m_phy_frames.insert({phy_frame.m_phy_id,std::move(phy_frame)});
}

FrameInfo::FrameInfo(FrameInfo&& other):
  m_acq_frame_nb(other.m_acq_frame_nb),
  m_frame_dim(other.m_frame_dim)
{
  for(auto &it: other.m_phy_frames)
    m_phy_frames.insert({it.first,std::move(it.second)});
  other.m_phy_frames.clear();
}
  /****************************************************************************
			    PhyFrame Info
  ****************************************************************************/
PhyFrameInfo::PhyFrameInfo(void* mmap_addr,ssize_t mmap_size,off_t frame_offset,
			   int phy_id,
			   const FrameDim &frame_dimension) :
  HwFrameInfo(),
  m_phy_id(phy_id),
  m_mmap_addr(mmap_addr),
  m_mmap_size(m_mmap_size)
{
  DEB_CONSTRUCTOR();
  DEB_PARAM() << DEB_VAR5(mmap_addr,mmap_size,frame_offset,phy_id,frame_dimension);
    
  frame_ptr = (char*)mmap_addr + frame_offset;
  frame_dim = frame_dimension;
  buffer_owner_ship = HwFrameInfoType::Managed;
}


PhyFrameInfo::PhyFrameInfo(PhyFrameInfo&& other) :
  HwFrameInfo(other),
  m_phy_id(other.m_phy_id),
  m_mmap_addr(other.m_mmap_addr),
  m_mmap_size(other.m_mmap_size)
{
  //transfert ownership of the mmap
  other.m_mmap_size = 0;
  other.m_mmap_addr = NULL;
}

PhyFrameInfo::~PhyFrameInfo()
{
  if(m_mmap_addr)
    munmap(m_mmap_addr,m_mmap_size);
}
  /****************************************************************************
			 Buffer Alloc Manager
  ****************************************************************************/
BufferMgr::BufferMgr(const char* tmpfs_path) :
  m_path(tmpfs_path),
  m_inotify_fd(-1),
  m_pipes{-1,-1},
  m_inotify_thread_id(-1),
  m_raw_file_reader_thread_id(-1),
  m_quit(false),
  m_wait(true),
  m_thread_running(0),
  m_memory_percent(0.1)
{
  DEB_CONSTRUCTOR();

  if(pipe(m_pipes))
    THROW_HW_ERROR(Error) << "Can't open pipe";

  m_inotify_fd = inotify_init();
  if(m_inotify_fd < 0)
    THROW_HW_ERROR(Error) << "Can't init inotify";

  _signal();
  
  if(pthread_create(&m_inotify_thread_id,NULL,BufferMgr::_inotify_watch_func,this))
    THROW_HW_ERROR(Error) << "Can't start inotify watching thread";

  if(pthread_create(&m_raw_file_reader_thread_id,NULL,BufferMgr::_file_reader_func,this))
    THROW_HW_ERROR(Error) << "Can't start file reader thread";

}

BufferMgr::~BufferMgr()
{
  m_cond.acquire();
  m_wait = m_quit = true;
  m_cond.broadcast();
  m_cond.release();

  if(m_pipes[1])
    close(m_pipes[1]);

  void *tReturn;
  if(m_inotify_thread_id != pthread_t(-1))
    pthread_join(m_inotify_thread_id,&tReturn);

  if(m_raw_file_reader_thread_id != pthread_t(-1))
    pthread_join(m_raw_file_reader_thread_id,&tReturn);
  
  if(m_pipes[0] >= 0)
    close(m_pipes[0]);
    
  if(m_inotify_fd >= 0)
    close(m_inotify_fd);
}


void BufferMgr::setMemoryPercent(double percent)
{
  DEB_MEMBER_FUNCT();

  if(percent >= 0. && percent <= 1.)
    m_memory_percent = percent;
  else
    THROW_HW_ERROR(Error) << "percent should be between 0. and 1.";

}

void BufferMgr::prepare()
{
  AutoMutex lock(m_cond.mutex());
  m_wait = true;
  _signal();
  while(m_thread_running)
    m_cond.wait();

  _clean_dir(m_path);
  
  
  m_frames.clear();
  m_frame_in_use.clear();
  m_frame_in_process.clear();
  m_notified_files.clear();
  m_pending_data_files.clear();
  m_current_data_files.clear();
  m_frame_in_use.clear();
  m_keep_nb_images = _calcNbMaxImages();
}

void BufferMgr::start()
{
  AutoMutex lock(m_cond.mutex());
  m_wait = false;
  _signal();
  m_cond.broadcast();
}

void BufferMgr::stop()
{
  AutoMutex lock(m_cond.mutex());
  m_wait = true;
  _signal();
  while(m_thread_running)
    m_cond.wait();
}


void BufferMgr::setNbBuffers(int)
{
  DEB_MEMBER_FUNCT();
}

void BufferMgr::getNbBuffers(int& nb_buffers)
{
  nb_buffers = m_keep_nb_images;
}

void BufferMgr::setNbConcatFrames(int nb_concat_frames)
{
  DEB_MEMBER_FUNCT();
  if(nb_concat_frames != 1)
    THROW_HW_ERROR(NotSupported) << "Not managed";
}

void BufferMgr::getNbConcatFrames(int& nb_concat_frames)
{
  nb_concat_frames = 1;
}

void BufferMgr::getMaxNbBuffers(int& max_nb_buffers)
{
  max_nb_buffers = _calcNbMaxImages();
}

void BufferMgr::getFrameInfo(int acq_frame_nb, HwFrameInfo& info)
{
  /*@todo need to reconstruct final frame
   * should not go thow this code 
   * except if ask unreconstruct frame
   */
  DEB_MEMBER_FUNCT();
  
  AutoMutex lock(m_cond.mutex());
  auto frame_info = m_frames.find(acq_frame_nb);
  if(frame_info == m_frames.end())
    THROW_HW_ERROR(Error) << "Frame not available";
  m_frame_in_use.insert(frame_info->second->m_acq_frame_nb);

  info.acq_frame_nb = frame_info->first;
  info.frame_dim = frame_info->second->m_frame_dim;
  m_frame_in_use.insert(frame_info->first);
  //info.frame_timestamp = frame_info->second->frame_timestamp;
  //info.valid_pixels = frame_info->second->valid_pixels;
  //info.buffer_owner_ship = HwFrameInfo::Shared;

  //Do a copy for asynchronous request
  // if(posix_memalign(&info.frame_ptr,16,info.frame_dim.getMemSize()))
  //  THROW_HW_ERROR(Error) << "Can not allocate frame";

  //@todo this can't work
  //  memcpy(info.frame_ptr,frame_info->second.frame_ptr,info.frame_dim.getMemSize());
}

void BufferMgr::registerFrameCallback(HwFrameCallback& frame_cb)
{
  HwFrameCallbackGen::registerFrameCallback(frame_cb);
}
void BufferMgr::unregisterFrameCallback(HwFrameCallback& frame_cb)
{
  HwFrameCallbackGen::unregisterFrameCallback(frame_cb);
}



bool BufferMgr::is_running()
{
  //return false;			// for now
  DEB_MEMBER_FUNCT();
  AutoMutex lock(m_cond.mutex());
  DEB_RETURN() << DEB_VAR3(m_frame_in_use.size(),m_current_data_files.size(),
			   m_pending_data_files.size());
 
     
  return (!m_frame_in_use.empty() ||
	  !m_current_data_files.empty() ||
	  !m_pending_data_files.empty());
}

int BufferMgr::get_acquired_frames()
{
  AutoMutex lock(m_cond.mutex());
  auto it = m_frames.begin();
  return it->first + m_frames.size();
}

int BufferMgr::_calcNbMaxImages()
{
  DEB_MEMBER_FUNCT();

  struct statvfs fsstat;
  if(statvfs(m_path.c_str(),&fsstat))
    THROW_HW_ERROR(Error) << "Can't figure out what is the size of that filesystem: " 
			  << m_path;


  unsigned long long nb_block_for_image = m_frame_dim.getMemSize() / fsstat.f_bsize;
  unsigned long long aTotalImage = fsstat.f_blocks / nb_block_for_image;
  int max_nb_images = std::min(aTotalImage * m_memory_percent * 100,::pow(2,31));
  DEB_RETURN() << DEB_VAR1(max_nb_images);
  return max_nb_images;
}


void BufferMgr::_inotify_watch()
{
  DEB_MEMBER_FUNCT();
  ++m_thread_running;
  struct pollfd fds[2];
  fds[0].fd = m_pipes[0];
  fds[0].events = POLLIN;
  fds[1].fd = m_inotify_fd;
  fds[1].events = POLLIN;
  while(!m_quit)
    {
      poll(fds,2,-1);
      if(fds[1].revents)
	{
	  char buffer[EVENT_BUF_LEN];
	  int length = read(m_inotify_fd,
			    buffer,sizeof(buffer));
	  char *aPt = buffer;
	  while(length > 0)
	    {
	      struct inotify_event *event = (struct inotify_event*)aPt;
	      if(event->len)
		{
		  if(event->mask & (IN_CREATE|IN_MOVED_TO|IN_CLOSE_WRITE)) // file creation
		    {
		      const char* filename = event->name;
		      auto it = m_inotify_wd.find(event->wd);
		      const std::string& base_path = it->second;
		      std::string fullPath = base_path + "/";
		      fullPath += filename;
		      if(event->mask & IN_ISDIR)
			{
			  std::string subpath = filename;
			  if(subpath == "." || subpath == "..")
			    continue;

			  _add_watch(fullPath);
			}
		      else
			{
			  DEB_TRACE() << DEB_VAR1(event->mask);
			  _check_new_data_file(fullPath);
			}
		    }
		  else if(event->mask & IN_MODIFY &&
			  !(event->mask & IN_ISDIR))
		    {
		      AutoMutex lock(m_cond.mutex());
		      m_cond.broadcast(); // just signal thread
		    }
		}
	      aPt += EVENT_SIZE + event->len;
	      length -= EVENT_SIZE + event->len;
	    }
	}

      if(fds[0].revents)	// flush pipe
	{
	  char buffer[1024];
	  if(!read(m_pipes[0],buffer,sizeof(buffer)))
	    break;
	}
      
      if(m_wait)
	{
	  AutoMutex lock(m_cond.mutex());
	  bool broadcast_flag = true;
	  if(m_quit) break;
	  while(m_wait && !m_quit)
	    {
	      _stop_watch();
	      --m_thread_running;
	      if(broadcast_flag)
		{
		  m_cond.broadcast();
		  broadcast_flag = false;
		}
	      DEB_TRACE() << "wait";
	      m_cond.wait();
	      DEB_TRACE() << "run";
	      ++m_thread_running;
	    }
	  if(!m_quit) _start_watch();
	}
    }
}

void BufferMgr::_start_watch()
{
  DEB_MEMBER_FUNCT();
  
  _add_watch(m_path);
}

void BufferMgr::_add_watch(const std::string& new_path)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(new_path);
  
  int new_inotify = inotify_add_watch(m_inotify_fd,
				      new_path.c_str(),
				      WATCH_MASK);
  auto it = m_inotify_wd.insert({new_inotify,new_path});
  if(!it.second)
    return;			// already insert
  //Register recursively directory
  const char *path = new_path.c_str();
  DIR* aWatchDir = opendir(path);
  DEB_TRACE() << DEB_VAR2(path,aWatchDir);
  if(aWatchDir)
    {
      while(1)
	{
	  char fullPath[1024];
	  struct dirent* result = readdir(aWatchDir);
	  if(!result) break;

	  std::string subpath = result->d_name;
	  if(subpath == "." || subpath == "..")
	    continue;

	  DEB_TRACE() << DEB_VAR1(subpath);
	  
	  snprintf(fullPath,sizeof(fullPath),
		   "%s/%s",path,result->d_name);
	  struct stat sb;
	  if(stat(fullPath,&sb) == 0 && S_ISDIR(sb.st_mode)) // is a directory
	    _add_watch(fullPath);
	  else
	    _check_new_data_file(fullPath);
	}
      closedir(aWatchDir);
    }

}

void BufferMgr::_signal()
{
  write(m_pipes[1],"|",1);
}

void BufferMgr::_stop_watch()
{
  for(auto it: m_inotify_wd)
    inotify_rm_watch(m_inotify_fd,it.first);

  m_inotify_wd.clear();
}


void BufferMgr::_check_new_data_file(const std::string& fullPath)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(fullPath);
  
  //check that filename is from good type
  auto pos = fullPath.find(FILE_EXTENTION);
  auto fullpath_size = fullPath.size();

  if(pos + FILE_EXTENTION.size() == fullpath_size)
    {
      auto data_file = std::make_shared<DataFile>(fullPath.c_str(),m_frame_dim);
      AutoMutex lock(m_cond.mutex());
      auto it = m_notified_files.insert(fullPath);
      if(!it.second) return;			// already notify
      DEB_TRACE() << "Added to list: " << DEB_VAR1(fullPath);
      m_pending_data_files.push_back(data_file);
      m_cond.broadcast();
    }
}

  
void BufferMgr::_clean_dir(const std::string& aPath)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(aPath);
  
  //Clean directory file
  const char *path = aPath.c_str();
  DIR* aWatchDir = opendir(path);
  if(aWatchDir)
    {
      while(1)
	{
	  char fullPath[1024];
	  struct dirent* result = readdir(aWatchDir);
	  if(!result) break;
	  std::string subpath = result->d_name;
	  if(subpath == "." || subpath == "..")
	    continue;
	  
	  snprintf(fullPath,sizeof(fullPath),
		   "%s/%s",path,result->d_name);

	  struct stat sb;
	  if(stat(fullPath,&sb) == 0 && S_ISDIR(sb.st_mode)) // is a directory
	    {
	      _clean_dir(fullPath);
	      rmdir(fullPath);
	    }
	  else
	    unlink(fullPath);
	}
      closedir(aWatchDir);
    }
}

void BufferMgr::_file_reader()
{
  DEB_MEMBER_FUNCT();
  
  AutoMutex lock(m_cond.mutex());
  ++m_thread_running;
  while(!m_quit)
    {
      bool broadcast_flag = true;
      while(!m_quit &&
	    (m_wait || m_pending_data_files.empty()))
	{
	  --m_thread_running;
	  if(broadcast_flag)
	    {
	      m_cond.broadcast();
	      broadcast_flag = false;
	    }
	  m_cond.wait();
	  ++m_thread_running;
	}

      if(m_quit) break;
      std::list<PhyFrameInfo> new_frames;
      {
	AutoMutexUnlock unlock(lock);
	int nb_ends = 0;
	for(auto cur_file: m_current_data_files)
	  {
	    bool end = cur_file->get_next(new_frames);
	    if(end)
	      ++nb_ends;
	  }
	if(nb_ends == m_current_data_files.size())
	  m_current_data_files.clear();
      }
      if(!m_current_data_files.empty() && new_frames.empty())
	m_cond.wait(1.);		// wait max 1 sec or a new event from inotify
      
      int nb_phy_file = ::ceil(m_frame_dim.getMemSize() / FrameInfo::PHY_SIZE / 2.);
      
      if(m_current_data_files.size() < nb_phy_file &&
	 !m_pending_data_files.empty())
	{
	  auto datafile = m_pending_data_files.front();
	  m_pending_data_files.pop_front();

	  bool end_file;
	  {
	    AutoMutexUnlock unlock(lock);
	    end_file = datafile->get_next(new_frames);
	  }
	  
	  while(!m_wait && new_frames.empty())
	    {
	      end_file = datafile->get_next(new_frames);
	      if(end_file) break;
	      else if(new_frames.empty())
		m_cond.wait();
	    }
	  
	  if(!end_file)
	    m_current_data_files.push_back(datafile);
	  DEB_TRACE() << DEB_VAR1(m_pending_data_files.size());
	}

      bool continueAcq = true;
      while(continueAcq && !new_frames.empty())
	{
	  PhyFrameInfo& phy_frame_info = new_frames.front();
	  std::shared_ptr<FrameInfo> newFrame =
	    std::make_shared<FrameInfo>(phy_frame_info.acq_frame_nb,
					m_frame_dim);
	  auto it = m_frames.insert({phy_frame_info.acq_frame_nb,
	  			     newFrame});

	  std::shared_ptr<FrameInfo> frame_info = it.first->second;
	  frame_info->add(phy_frame_info);
	  new_frames.pop_front();
	  if(frame_info->is_complete())
	    {
	      m_frame_in_use.insert(frame_info->m_acq_frame_nb);

	      
	      HwFrameInfo f_info;
	      f_info.acq_frame_nb = frame_info->m_acq_frame_nb;
	      f_info.frame_dim = frame_info->m_frame_dim;
	      //@todo limit the speed of pushing frames
	      {
		AutoMutexUnlock unlock(lock);
		DEB_TRACE() << DEB_VAR1(f_info);
		continueAcq = newFrameReady(f_info);
	      }
	      
	      // Limit to MAX_PROCESSED_FRAMES frames before (reconstruction)
	      // the process queue
	      while(m_frame_in_use.size() > MAX_PROCESSED_FRAMES)
		if(!m_cond.wait(3))
		  break;	// break on timeout

	      m_wait = !continueAcq;
	      //Memory management
	      if(m_frames.size() > m_keep_nb_images)
		{
		  auto old_it = new_frames.begin();
		  while(!m_wait && m_frame_in_use.find(old_it->acq_frame_nb) !=
			m_frame_in_use.end())
		    m_cond.wait();
		  new_frames.erase(old_it);
		}
	      // Limit to MAX_PROCESSED_FRAMES frames in process queue
	      while(m_frame_in_process.size() > MAX_PROCESSED_FRAMES)
		if(!m_cond.wait(3))
		  break;	// break on timeout
	    }
	}
      
    }
}
