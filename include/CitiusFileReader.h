//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#pragma once
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <list>
#include <map>
#include <set>
#include <memory>

#include "processlib/Data.h"

#include "lima/Debug.h"
#include "lima/Exceptions.h"
#include "lima/HwFrameInfo.h"
#include "lima/HwFrameCallback.h"
#include "lima/HwBufferCtrlObj.h"


namespace lima
{
  namespace Citius
  {
    class _ReconstructionTask;
    class PhyFrameInfo : public HwFrameInfo
    {
      DEB_CLASS_NAMESPC(DebModCamera, "PhyFrameInfo", "Citius");
    public:
      PhyFrameInfo(void* mmap_addr,ssize_t mmap_size,off_t frame_offset,
		   int phy_id,
		   const FrameDim&);
      PhyFrameInfo(PhyFrameInfo&&);
      ~PhyFrameInfo();

      int 	m_phy_id;
    private:
      PhyFrameInfo(const PhyFrameInfo&) = delete;	// just forbidden.
      PhyFrameInfo& operator =(const PhyFrameInfo&) = delete;

      void*	m_mmap_addr;
      ssize_t	m_mmap_size;
    };

    class DataFile;
    std::ostream& operator<<(std::ostream& os,const DataFile&);
    class DataFile
    {
      DEB_CLASS_NAMESPC(DebModCamera, "DataFile", "Citius");

    public:
      DataFile(const char* filepath,const FrameDim&);
      ~DataFile();

      DataFile(DataFile&&);
      
      bool get_next(std::list<PhyFrameInfo>&);

      int get_phy_id() const;

      friend std::ostream& operator<<(std::ostream& os, const DataFile&);
    private:
      enum MODE {UNINITIALIZED,TRAIN,FRAME,END};
      
      std::string	m_file_path;
      int		m_file_fd;
      MODE		m_mode;
      off_t		m_next_seek;
      unsigned long	m_nb_frames_to_read;
      FrameDim		m_frame_dim;
      uint32_t 		m_packet_header_info;
    };

    class FrameInfo //: public sideband::Data
    {
      DEB_CLASS_NAMESPC(DebModCamera, "FrameInfo", "Citius");
    public:
      FrameInfo(int acq_frame_nb,FrameDim frame_dim);
      ~FrameInfo();

      bool is_complete() const;
      void add(PhyFrameInfo&);
      
      FrameInfo(FrameInfo&&);

      static const int PHY_SIZE = 196608;

      int	m_acq_frame_nb;
      FrameDim	m_frame_dim;

      std::map<int,PhyFrameInfo>	m_phy_frames;      
     private:
      FrameInfo(const FrameInfo&) = delete;	// just forbidden.
      FrameInfo& operator =(const FrameInfo&) = delete;
    };

    class BufferMgr : public HwBufferCtrlObj, public HwFrameCallbackGen
    {
       DEB_CLASS_NAMESPC(DebModCamera, "BufferMgr", "Citius");
    public:
      BufferMgr(const char* tmpfs_path);
      ~BufferMgr();

      void setMemoryPercent(double);
      void prepare();
      void start();
      void stop();
      bool isStopped() const;
      int getLastAcquiredFrame() const;
     
      virtual void setFrameDim(const FrameDim& frame_dim) {m_frame_dim = frame_dim;}
      virtual void getFrameDim(FrameDim& frame_dim) {frame_dim = m_frame_dim;}

      virtual void setNbBuffers(int nb_buffers);
      virtual void getNbBuffers(int& nb_buffers);

      virtual void setNbConcatFrames(int nb_concat_frames);
      virtual void getNbConcatFrames(int& nb_concat_frames);

      virtual void getMaxNbBuffers(int& max_nb_buffers);

      /// Returns a pointer to the buffer at the specified location
      virtual void *getBufferPtr(int buffer_nb, int concat_frame_nb = 0) {return NULL;}
      /// Returns a pointer to the frame at the specified location
      virtual void *getFramePtr(int acq_frame_nb) {return NULL;}

      /// Returns the start timestamp
      virtual void getStartTimestamp(Timestamp& start_ts) {start_ts = m_start_time;}
      /// Returns some information for the specified frame number such as timestamp
      virtual void getFrameInfo(int acq_frame_nb, HwFrameInfoType& info);

      virtual void registerFrameCallback(HwFrameCallback& frame_cb);
      virtual void unregisterFrameCallback(HwFrameCallback& frame_cb);

      bool is_running();
      int get_acquired_frames();
    private:
      friend class _ReconstructionTask;
      
      int _calcNbMaxImages();
      static void* _inotify_watch_func(void *arg)
      {
	((BufferMgr*)arg)->_inotify_watch();
	return NULL;
      }
      void _inotify_watch();

      static void* _file_reader_func(void *arg)
      {
	((BufferMgr*)arg)->_file_reader();
	return NULL;
      }
      void _file_reader();
      void _start_watch();
      void _stop_watch();
      void _check_new_data_file(const std::string&);
      void _add_watch(const std::string&);
      void _signal();
      void _clean_dir(const std::string& fullPath);

      std::string				m_path;
      int					m_inotify_fd;
      int					m_pipes[2];
      FrameDim					m_frame_dim;
      std::map<int,std::shared_ptr<FrameInfo>>	m_frames; // frame in memory
      std::multiset<int>			m_frame_in_use; // frame in use before reconstruction
      std::set<void*>				m_frame_in_process; // frame in process queue
      std::set<std::string>			m_notified_files;
      std::list<std::shared_ptr<DataFile>>	m_pending_data_files;
      std::list<std::shared_ptr<DataFile>>	m_current_data_files;
      double					m_memory_percent;
      int					m_keep_nb_images;
      Timestamp					m_start_time;
      Cond					m_cond;
      bool					m_quit;
      bool					m_wait;
      int					m_thread_running;
      pthread_t					m_inotify_thread_id;
      std::map<int,const std::string>		m_inotify_wd;
      pthread_t					m_raw_file_reader_thread_id;

      static const size_t EVENT_SIZE = sizeof(struct inotify_event);
      static const size_t EVENT_BUF_LEN = (1024 * (EVENT_SIZE + 16));
      static const std::string FILE_EXTENTION;
    };
  }
}
