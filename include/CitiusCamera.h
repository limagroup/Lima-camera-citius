//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#pragma once
#include <iostream>
#include <vector>

#include <pistache/client.h>
#include <pistache/http.h>
#include <pistache/net.h>

#include "lima/Debug.h"
#include "lima/Exceptions.h"

namespace lima
{
  namespace Citius
  {
    static const char* LIMA_EXTRA_PATH = "lima_tmp";
    static const char* LIMA_TMP_PATH = "/citius/img/lima_tmp";
    class Camera
    {
      DEB_CLASS_NAMESPC(DebModCamera, "Camera", "Citius");

    public:
      enum AcquisitionStatus {NOT_READY,READY};
      enum CoolingStatus {NOT_CONNECTED,DISABLED,COOLING,COOLED,ERROR};
	
      Camera(const std::string& url);
      ~Camera();

      AcquisitionStatus acq_status();
      CoolingStatus cooling_status();

      int get_nb_acq_frames();
      void set_nb_acq_frames(int,int);
      
      int get_nb_sum_frames();
      void set_nb_sum_frames(int);

      int get_nb_filewriter_train();
      void set_nb_filewriter_train(int);
      
      void prepareAcq();
      void startAcq();
      void stopAcq();
    private:
      std::string				m_base_url;
      Pistache::Http::Experimental::Client	m_client;

      typedef Pistache::Async::Promise<Pistache::Http::Response> AsynResponse;

      inline AcquisitionStatus _string_2_acquisition_status(const std::string&);
      inline CoolingStatus _string_2_cooling_status(const std::string&);
      inline void _raw_get_value_string(const std::string& url_suffix,
					std::vector<AsynResponse>&,std::string&);
      inline void _raw_set_string_value(const std::string& url_suffix,
					std::vector<AsynResponse>&,const std::string&,
					std::string& error_msg);
      inline void _wait_responses(std::vector<AsynResponse>&,int timeout = 30);
    };
  }
}
