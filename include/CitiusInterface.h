//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2024
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#pragma once
#include "lima/HwInterface.h"

namespace lima
{
  namespace Citius
  {
    class Camera;
    class BufferMgr;
    class DetInfoCtrlObj: public HwDetInfoCtrlObj
    {
      DEB_CLASS_NAMESPC(DebModCamera, "DetInfoCtrlObj", "Citius");

    public:
      DetInfoCtrlObj(Camera& cam);
      virtual ~DetInfoCtrlObj();

      virtual void getMaxImageSize(Size& max_image_size);
      virtual void getDetectorImageSize(Size& det_image_size);

      virtual void getDefImageType(ImageType& def_image_type);
      virtual void getCurrImageType(ImageType& curr_image_type);
      virtual void setCurrImageType(ImageType curr_image_type);

      virtual void getPixelSize(double& x_size,double &y_size);
      virtual void getDetectorType(std::string& det_type);
      virtual void getDetectorModel(std::string& det_model);

      virtual void registerMaxImageSizeCallback(HwMaxImageSizeCallback&) {}

      virtual void unregisterMaxImageSizeCallback(HwMaxImageSizeCallback&) {}
    private:
      Camera& m_cam;
    };

    
    class SyncCtrlObj: public HwSyncCtrlObj
    {
      DEB_CLASS_NAMESPC(DebModCamera, "SyncCtrlObj", "Citius");

    public:

      SyncCtrlObj(Camera& cam);
      virtual ~SyncCtrlObj();

      void prepareAcq();
      
      virtual bool checkTrigMode(TrigMode trig_mode);
      virtual void setTrigMode(TrigMode trig_mode);
      virtual void getTrigMode(TrigMode& trig_mode);

      virtual void setExpTime(double exp_time);
      virtual void getExpTime(double& exp_time);

      virtual void setLatTime(double lat_time);
      virtual void getLatTime(double& lat_time);

      virtual void setNbHwFrames(int nb_frames);
      virtual void getNbHwFrames(int& nb_frames);

      virtual void getValidRanges(ValidRangesType& valid_ranges);

    private:
      Camera& m_cam;
      int m_nb_acc_frames;
      int m_nb_frames;
    };

/*******************************************************************
 * \class Reconstruction
 * \brief Control object providing reconstruction interface
 *******************************************************************/
    class Reconstruction: public HwReconstructionCtrlObj
    {
      DEB_CLASS_NAMESPC(DebModCamera, "ReconstructionCtrlObj", "Citius");
    public:
      Reconstruction(BufferMgr&);
      ~Reconstruction();

      virtual LinkTask* getReconstructionTask();
    private:
      LinkTask* m_reconstruction_task;
    };
    
    class Interface: public HwInterface
    {
      DEB_CLASS_NAMESPC(DebModCamera, "CitiusInterface", "Citius");

    public:
      Interface(const std::string& url);
      virtual ~Interface();

      //- From HwInterface
      virtual void getCapList(CapList&) const;
      virtual void reset(ResetLevel reset_level);
      virtual void prepareAcq();
      virtual void startAcq();
      virtual void stopAcq();
      virtual void getStatus(StatusType& status);
      virtual int getNbHwAcquiredFrames();


      virtual bool firstProcessingInPlace() const 
      {return false;}

    private:
      std::unique_ptr<Camera>		m_cam;
      CapList				m_cap_list;
      DetInfoCtrlObj			m_det_info;
      SyncCtrlObj			m_sync;
      std::unique_ptr<BufferMgr>	m_buffer;
      Reconstruction			m_reconstruction;
    };
  }
}
